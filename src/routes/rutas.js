const express = require('express');
const router = express.Router(); //creo un objeto en una variable para almacenar rutas.

const User = require('../models/User'); //requiero el modelo Tasks de la carpeta Models

// router.get('/', async(req, res)=>{
//   const tasks = await Task.find();
//   res.json(tasks);
// });

router.post('/', async(req, res)=>{
    console.log(req.body);
    const user = new User(req.body);
    await user.save();
    res.json({
      status: 'Usuario Guardado'
    });
});

// router.put('/:id', async(req, res)=>{
//   await Task.findByIdAndUpdate(req.params.id, req.body);
//   res.json({
//       status : 'Tarea Actualizada'
//   });
// });

// router.delete('/:id', async(req, res)=>{
//   await Task.findByIdAndRemove(req.params.id);
//   res.json({
//       status : 'Tarea Borrada'
//   });
// })

module.exports = router; //exporto el objeto.