const mongoose = require('mongoose');
const { Schema } = mongoose; //requiero solo una parte de mongosee

const Task = new Schema({
    title : String,
    description: String
});

module.exports = mongoose.model('Task', Task); //exporto el modelo