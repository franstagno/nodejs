const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');

const app = express(); //ejecuta el framework
mongoose.connect('mongodb://localhost:27017/mevn-database', {useNewUrlParser: true})
.then(db => console.log('DB is connected'))
.catch(err => console.error(err));

//Settings
app.set('port', process.env.PORT || 3000); 

//Middlewares
app.use(morgan('dev')); //informacion en consola del navegador
app.use(express.urlencoded({ // PONER SI O SI
	extended : false
}))
app.use(express.json()); // el servidor entiende los JSON

//Routes
app.use(require('./routes/rutas')); //requiero el archivo

//Static Files (archivos que mandamos al frontend)
app.use(express.static(__dirname + '/public'));

//Server is Listening
app.listen(app.get('port'), ()=>{
    console.log('Server on port', app.get('port'));
});